import React, {Dispatch, SetStateAction, useEffect, useState} from 'react';
import googleImage from '../images/google.webp'
import {startPage} from "../functions/start";


type IModel = {
    SetSecondPage:Dispatch<SetStateAction<boolean>>
}

const Google = ({SetSecondPage}:IModel) => {
    const [question, setQuestion] = useState<string | undefined>('');
    const [click, setClick] = useState<boolean>(true);
    const [modal, setModal] = useState<boolean>(false);
    useEffect(() => {
    }, [question])
    let startText = 'Гугл или яндекс, что лучше?';
    useEffect(() => {
        if (question === startText) {
            setModal(true)
            setTimeout(() => {
                SetSecondPage(true);
            }, 1000)
        }
    }, [question, startText])

    function handleClick() {
        if (click) {
            setClick(false);
            startPage(startText, setQuestion)
        }
    }

    return (
        <section className="h-screen flex flex-col justify-center items-center">
            <img alt={"google"} src={googleImage} className="max-w-[272px]"/>
            <article
                className={modal ? "my-5 px-2 px-4 border rounded-[20px] max-w-[600px] w-full" : "my-5 px-2 px-4 border rounded-full max-w-[600px] w-full"}>
                <section
                    className={modal ? "flex items-center w-full border-b-[1px] border-gray-200 h-[48px]" : "flex items-center w-full  h-[48px]"}>
                    <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 1024 1024"
                         className="text-black cursor-pointer" height="20" width="20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0 0 11.6 0l43.6-43.5a8.2 8.2 0 0 0 0-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                    </svg>
                    <div
                        className="cursor-text w-full h-[27px] flex-1 mx-3 overflow-hidden text-xs md:text-base text-black whitespace-nowrap ">
                        <span className="Typewriter__wrapper">{question}</span>
                        <span className="Typewriter__cursor ">|</span>

                    </div>
                    <button>
                        <svg width={"24px"} height={"24px"} focusable="false" viewBox="0 0 192 192"
                             xmlns="http://www.w3.org/2000/svg">
                            <rect fill="none" height="192" width="192"></rect>
                            <circle fill="#34a853" cx="144.07" cy="144" r="16"></circle>
                            <circle fill="#4285f4" cx="96.07" cy="104" r="24"></circle>
                            <path fill="#ea4335"
                                  d="M24,135.2c0,18.11,14.69,32.8,32.8,32.8H96v-16l-40.1-0.1c-8.8,0-15.9-8.19-15.9-17.9v-18H24V135.2z"></path>
                            <path fill="#fbbc04"
                                  d="M168,72.8c0-18.11-14.69-32.8-32.8-32.8H116l20,16c8.8,0,16,8.29,16,18v30h16V72.8z"></path>
                            <path fill="#4285f4"
                                  d="M112,24l-32,0L68,40H56.8C38.69,40,24,54.69,24,72.8V92h16V74c0-9.71,7.2-18,16-18h80L112,24z"></path>

                        </svg>
                    </button>
                </section>
                {modal ?
                    <section className="my-2 flex items-center">
                        <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 1024 1024"
                             className="text-black mr-4 opacity-75 cursor-pointer" height="20" width="20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0 0 11.6 0l43.6-43.5a8.2 8.2 0 0 0 0-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                        </svg>
                        <p>Гугл или яндекс, что лучше?</p>
                    </section>
                    : ''}
            </article>
            <article className="flex items-center justify-center space-x-2 text-xs md:text-sm text-gray-1000"><span
                className="p-2 bg-gray-100 border border-blue-500 rounded-sm">Поиск в Google</span><span
                className="p-2 bg-gray-100 border border-gray-100 rounded-sm">Мне повезет!</span>
            </article>
            <button onClick={handleClick} className="mt-8 w-[50px] h-[50px] duration-300 hover:scale-110">
                <svg xmlns="http://www.w3.org/2000/svg" width="50px" height="50px" viewBox="-9 0 64 64" version="1.1">
                    <path
                        d="M1,61 C1,62.1 1.8,63 2.9,63 L43.1,35 C44.7,33.9 45,32.7 45,31.9 L45,31.9 C45,31.9 44.9,30.2 43.1,28.9 L2.9,1 C1.9,1 1,1.9 1,3 L1,61 L1,61 Z"
                        stroke="#6B6C6E" fill="none" strokeWidth="2">
                    </path>
                </svg>
            </button>
        </section>
    );
};

export default Google;