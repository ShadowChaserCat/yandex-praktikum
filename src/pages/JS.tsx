import React from 'react';
import schoolMaksimka from "../images/schoolMaksimka.jpg";
import maksimkaNow from "../images/IMG_4822.webp";
import {NavLink} from "react-router-dom";

const Js = () => {
    return (
        <section className="mt-8 max-w-[640px] mx-auto">
            <article>
                <h1 className="text-3xl font-bold">Задачки по js :)</h1>
                <p className="mt-8 text-lg">1) Что будет выведено в консоль? <code>console.log(1_000 + 1_500);</code>
                    <ul>
                        <li className="text-sm">2500</li>
                        <li className="text-sm">2_500</li>
                        <li className="text-sm">1_0001_500</li>
                        <li className="text-sm">1_5001_000</li>
                    </ul>
                </p>
                <p className="mt-8 text-lg">2) Самый быстрый способ перебрать массив?
                    <ul>
                        <li className="text-sm">for</li>
                        <li className="text-sm">for of</li>
                        <li className="text-sm">while</li>
                        <li className="text-sm">forEach</li>
                    </ul>
                </p>
                <p className="mt-8 text-lg">3) Как быстрее всего очистить массив?
                    <ul>
                        <li className="text-sm">2500</li>
                        <li className="text-sm">2_500</li>
                        <li className="text-sm">1_0001_500</li>
                        <li className="text-sm">1_5001_000</li>
                    </ul>
                </p>
                <p className="mt-8 text-lg">4) Какой будет вывод в консоль? <code>console.log((7,9-6) * 2 + '2' - 5 );</code>
                    <ul>
                        <li className="text-sm">57</li>
                        <li className="text-sm">-1.1999999999999993</li>
                        <li className="text-sm">0.8000000000000007</li>
                        <li className="text-sm">3</li>
                    </ul>
                </p>
            </article>
        </section>
    );
};

export default Js;