import React, { useState} from 'react';
import Google from "../component/Google";
import GoogleAnswer from "../component/GoogleAnswer";

const Main = () => {
    const [secondPage, setSecondPage] = useState<boolean>(false);
    return (
        <>
            {secondPage? <GoogleAnswer/>: <Google SetSecondPage={setSecondPage}/>}
        </>
    );
};

export default Main;