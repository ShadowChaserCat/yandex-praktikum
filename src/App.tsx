import React from 'react';

import './App.css';
import Main from "./pages/Main";
import {Route, Routes} from "react-router-dom";
import JS from "./pages/JS";

function App() {
    document.title = 'ТЗ'
  return (
      <main>
          <Routes>
              <Route path="/" element={<Main/>}/>
              <Route path="/js" element={<JS/>}/>
          </Routes>
      </main>
  );
}

export default App;
