import {Dispatch, SetStateAction} from "react";

let i = 0;
let currentText = '';

let speed = 100;
export function startPage(txt:string, UseState:Dispatch<SetStateAction<string | undefined>>) {
    if (i < txt.length) {
        currentText += txt[i];
        i++;
        setTimeout( () => {
            startPage(txt, UseState)
        }, speed);
        UseState(currentText)
    }
}